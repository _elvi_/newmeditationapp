package com.example.meditationapp.main_screen.home_fragment

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface NetworkManagerFeelings {

    companion object {
        val instance = Retrofit.Builder()
            .baseUrl("http://mskko2021.mad.hakta.pro/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NetworkManagerFeelings::class.java)
    }

    @GET ("feelings")
    fun getFeelings() : Call<Feelings>
}