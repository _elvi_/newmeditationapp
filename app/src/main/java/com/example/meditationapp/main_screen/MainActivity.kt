package com.example.meditationapp.main_screen

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.meditationapp.R
import com.example.meditationapp.main_screen.sound_fragment.SoundFragment
import com.example.meditationapp.databinding.ActivityMainBinding
import com.example.meditationapp.main_screen.home_fragment.HomeFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.bottomNavigation.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_home -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, HomeFragment())
                        .commit()
                }
                R.id.action_sound -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, SoundFragment())
                        .commit()
                }
            }
            true
        }

        //оставляем, т.к. при открытии активки, что-то нужно показывать
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, HomeFragment())
            .commit()
    }
}