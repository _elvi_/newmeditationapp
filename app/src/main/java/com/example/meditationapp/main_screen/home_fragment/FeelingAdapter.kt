package com.example.meditationapp.main_screen.home_fragment

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.example.meditationapp.R
import com.example.meditationapp.databinding.FeelingsItemBinding
import com.squareup.picasso.Picasso

class FeelingAdapter(private val context: Context, private val feelingsData: List<FeelingsData>) :
    RecyclerView.Adapter<FeelingAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.feelings_item, parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = FeelingsItemBinding.bind(holder.itemView)
        val feeling = feelingsData[position]

        println(feeling.image)
        println(feeling.title)

//        Picasso.get()
//            .load(feeling.image)
//            .into(binding.feelingsImage)

        Glide
            .with(holder.itemView.context)
            .load(feeling.image)
            .placeholder(R.drawable.cute_coala)
            .into(binding.feelingsImage)

        binding.feelingsTitle.text = feeling.title
    }

    override fun getItemCount(): Int = feelingsData.size
}