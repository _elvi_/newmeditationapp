package com.example.meditationapp.main_screen.home_fragment

data class Feelings(
    val success: Boolean,
    val data: List<FeelingsData>
)