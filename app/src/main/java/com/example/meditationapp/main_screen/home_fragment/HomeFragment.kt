package com.example.meditationapp.main_screen.home_fragment

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.meditationapp.R
import com.example.meditationapp.databinding.FragmentHomeBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment : Fragment(R .layout.fragment_home) {

    private lateinit var binding: FragmentHomeBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentHomeBinding.bind(view)

        binding.feelingsRecyclerView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        NetworkManagerFeelings.instance
            .getFeelings()
            .enqueue(object : Callback<Feelings> {
                override fun onResponse(call: Call<Feelings>, response: Response<Feelings>) {
                    Toast.makeText(requireContext(), response.body().toString(), Toast.LENGTH_SHORT).show()
                    if (response.body() != null) {
                        binding.feelingsRecyclerView.adapter = FeelingAdapter(requireContext(), response.body()!!.data)
                    }
                }

                override fun onFailure(call: Call<Feelings>, t: Throwable) {
                    Toast.makeText(requireContext(), t.localizedMessage, Toast.LENGTH_SHORT).show()
                    t.printStackTrace()
                }
            })
    }
}