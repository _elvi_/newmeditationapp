package com.example.meditationapp.main_screen.home_fragment

data class FeelingsData(
    val title: String,
    val image: String
)